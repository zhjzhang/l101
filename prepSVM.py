from classifiers import *
from math import ceil, factorial, log10
from tokenizer import *
import nltk
import os
import subprocess

BROWN = nltk.corpus.brown
TEST_SET = [u'cn20', u'cn24', u'cn03', u'cn11', u'cn25', u'cn05', u'cg35', u'cg25', u'cg61', u'cg24', u'cg40', u'cg57', u'cg55', u'cg05', u'cg17', u'cg65', u'cg63', u'cg22', u'cg36', u'cg50', u'cg75', u'cb16', u'cb10', u'cb15', u'cb18', u'cb05', u'cb01', u'ck10', u'ck08', u'ck27', u'ck29', u'ck16', u'ck18', u'ch06', u'ch20', u'ch13', u'ch21', u'ch17', u'ch04', u'ce18', u'ce15', u'ce22', u'ce06', u'ce32', u'ce33', u'ce20', u'ce26', u'cr03', u'cr02', u'cj42', u'cj60', u'cj10', u'cj44', u'cj30', u'cj67', u'cj02', u'cj37', u'cj03', u'cj36', u'cj21', u'cj76', u'cj71', u'cj62', u'cj40', u'cj14', u'cf13', u'cf30', u'cf46', u'cf27', u'cf01', u'cf17', u'cf40', u'cf24', u'cf25', u'cf44', u'cl06', u'cl05', u'cl21', u'cl09', u'cl12', u'ca30', u'ca44', u'ca16', u'ca28', u'ca23', u'ca05', u'ca04', u'ca41', u'ca12', u'cd14', u'cd13', u'cd03', u'cd07', u'cc02', u'cc03', u'cc06', u'cc14', u'cp29', u'cp09', u'cp19', u'cp20', u'cp24', u'cp11', u'cm05', u'cm06']
GENRES = list(set(map(lambda x: x[0:2], BROWN.fileids())))

INF = [u'belles_lettres', u'editorial', u'government', u'hobbies', u'learned', u'lore', u'news', u'religion', u'reviews']
INF_GENRES = []
for genre in BROWN.categories():
    if genre in INF:
        INF_GENRES.append(BROWN.fileids(categories=genre)[0][0:2])

def write_data_top_level(tokens):
    data = []
    for fid in BROWN.fileids():
        if fid[0:2] in INF_GENRES:
            data.append(("+1" + tokens[fid], fid))
        else:
            data.append(("-1" + tokens[fid], fid))
    training = open("training_top_level.dat", 'w')
    test = open("test_top_level.dat", 'w')
    for line, fid in data:
        if fid in TEST_SET:
            test.write("%s\n" % line)
        else:
            training.write("%s\n" % line)

def generate_training_data_10(tokens):
    out = {}
    out['fi'] = []
    for genre in GENRES:
        if genre in INF_GENRES:
            out[genre] = []
    for fid in filter(lambda x: not x in TEST_SET, BROWN.fileids()):
        for genre in GENRES:
            if genre in fid:
                genre = genre if genre in INF_GENRES else 'fi'
                out[genre].append("+1" + tokens[fid])
            else:
                genre = genre if genre in INF_GENRES else 'fi'
                out[genre].append("-1" + tokens[fid])
    return out

def generate_training_data(tokens):
    out = {}
    for genre in GENRES:
        out[genre] = []
    for fid in filter(lambda x: not x in TEST_SET, BROWN.fileids()):
        for genre in GENRES:
            if genre in fid:
                out[genre].append("+1" + tokens[fid])
            else:
                out[genre].append("-1" + tokens[fid])
    return out

def write_training_data(data):
    if not os.path.exists('training'):
        os.makedirs('training')
    for genre in data:
        with open("training/%s.dat" % genre, 'w') as f:
            for line in data[genre]:
                f.write("%s\n" % line) 

def generate_tokens():
    out = {}
    token_map = {}
    counter = 1
    for fid in BROWN.fileids():
        tokens = BROWN.words(fileids=fid)
        outlist = []
        seen = set()
        for token in tokens:
            if not token in token_map:
                token_map[token] = counter
                counter += 1
            if not token in seen:
                seen.add(token)
                outlist.append(token_map[token])
        outlist.sort()
        out[fid] = reduce(lambda x,y: x + y, \
            map(lambda x:" %d:1" % x, outlist))
    return out

def train_svm(data):
    if not os.path.exists('models'):
        os.makedirs('models')
    for genre in data:
        print "---------- Training genre %s ----------" % genre
        subprocess.call(["./svm_learn", "training/%s.dat" % genre, "models/%s" % genre])

def test_svm10(tokens):
    if not os.path.exists('test'):
        os.makedirs('test')
    if not os.path.exists('output'):
        os.makedirs('output')
    genres = INF_GENRES + ['fi']
    success = 0
    for fid in TEST_SET:
        test_id = fid if fid[0:2] in INF_GENRES else fid + 'fi'
        with open("test/%s.dat" % test_id, 'w') as f:
            f.write("+1 %s\n" % tokens[fid])
        results = []
        true_val = None
        for genre in genres:
            subprocess.call(["./svm_classify",\
                    "test/%s.dat" % test_id, "models/%s" % genre,\
                    "output/%s_%s.out" % (test_id, genre)])
            with open("output/%s_%s.out" % (test_id, genre), 'r') as f:
                value = float(f.readline())
                if genre in test_id:
                    true_value = value
                results.append(value)
        success += (1 if true_value == max(results) else 0)
    print "FINAL ACCURACY %0.3f" % (float(success) / len(TEST_SET))

def find_max(l):
    m = max(l)

if __name__ == "__main__":
    tokens = generate_tokens()
    """
    training = generate_training_data_10(tokens)
    write_training_data(training)
    train_svm(training)
    """
    test_svm10(tokens)

