from classifiers import *
from random import shuffle
from sklearn import svm
from tokenizer import *
import nltk
import numpy as np
import subprocess

BROWN = nltk.corpus.brown
GENRES = list(set(map(lambda x: x[0:2], BROWN.fileids())))

INF = [u'belles_lettres', u'editorial', u'government', u'hobbies', u'learned', u'lore', u'news', u'religion', u'reviews']
INF_GENRES = []
for genre in BROWN.categories():
    if genre in INF:
        INF_GENRES.append(BROWN.fileids(categories=genre)[0][0:2])

def generate_feature_map():
    feature_map = {}
    counter = 0
    for fid in BROWN.fileids():
        tokens = basic(fid)
        for token in tokens:
            if not token in feature_map:
                feature_map[token] = counter
                counter += 1
    return feature_map 

def generate_class_map():
    output = {}
    index = 0
    for genre in GENRES:
        if genre in INF_GENRES:
            output[genre] = index
            index += 1
        elif 'fi' in output:
            output[genre] = output['fi']
        else:
            output['fi'] = index
            output[genre] = index
            index += 1
    return output

def get_feature_vector(fid, feature_map):
    tokens = basic(fid)
    output = np.zeros(len(feature_map))
    for token in tokens:
        if token in feature_map:
            output[feature_map[token]] = 1
    return output

def train_svm(feature_map, class_map, fids):
    features = []
    classes = []
    for fid in fids:
        features.append(get_feature_vector(fid, feature_map))
        classes.append(class_map[fid[0:2]])
    lin_clf = svm.LinearSVC()
    lin_clf.fit(features, classes)
    return lin_clf

def test_svm(lin_clf, feature_map, class_map, test_set):
    output = lin_clf.predict(\
            map(lambda fid: get_feature_vector(fid, feature_map),\
            test_set))
    num_success = 0
    for i, fid in enumerate(test_set):
        if output[i] == class_map[fid[0:2]]:
            num_success += 1
    return float(num_success) / len(test_set)

def cross_validate(k=10):
    feature_map = generate_feature_map()
    class_map = generate_class_map()
    print class_map
    fids = BROWN.fileids()
    index = range(len(fids))
    shuffle(index)
    partitions = partition(index, k)
    results = []
    for i in range(k):
        training_indices = []
        for j in range(k):
            if not i == j:
                training_indices = training_indices + partitions[j]
        print "Training for %d-th partition" % i
        training_fids = map(lambda x: fids[x], training_indices)
        test_fids = map(lambda x: fids[x], partitions[i])
        lin_clf = train_svm(feature_map, class_map, training_fids)
        result = test_svm(lin_clf, feature_map, class_map, test_fids)
        results.append(result)
        print "%d-fold validation, svm, result: %0.3f" % (k, result)
    print "Average accuracy %0.3f" % (sum(results) / len(results))


def partition(lst, n):
    division = len(lst) / float(n)
    return [ lst[int(round(division * i)): int(round(division * (i + 1)))] for i in range(n) ]


if __name__ == "__main__":
    cross_validate()

