import nltk
import re
import string
from math import ceil
from random import choice

BROWN = nltk.corpus.brown

def basic(fileid):
    raw = BROWN.words(fileids=[fileid])
    regex = re.compile('[a-zA-Z0-9]')
    tokens = filter(lambda word: regex.search(word), raw)
    tokens = map(lambda word: word.lower(), raw)
    return tokens

if __name__ == "__main__":
    basic('cg22')
    counts = {}
    for fid in BROWN.fileids():
        if fid[0:2] in counts:
            counts[fid[0:2]] += 1
        else:
            counts[fid[0:2]] = 0
    print BROWN.categories()
    print counts
    total = 0
    for fid in counts:
        total += counts[fid]
    print total / len(counts)
    test = []
    for genre in BROWN.categories():
        print genre
        print BROWN.fileids(categories=genre)[0][0:2]
    for genre in BROWN.categories():
        tmp = []
        fids = BROWN.fileids(categories=genre)
        number = ceil(len(fids) / 5.0)
        while len(tmp) < number:
            fid = choice(fids)
            tmp.append(fid)
            fids.remove(fid)
        test = test + tmp


