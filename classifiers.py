from collections import defaultdict
from math import log
import json
import nltk
import tokenizer

BROWN = nltk.corpus.brown
GENRES = nltk.corpus.brown.categories()
TIE = None

class MultinoulliNBClassifierFactory(object):
    def __init__(self, training_set):
        # Returns negative log of the empirical prob. that a single appearance
        # of a word is in a positive review.
        token_counts = {}
        genre_counts = {}
        for genre in GENRES:
            fileids = BROWN.fileids(categories=genre)
            num_files = 0
            counts = defaultdict(lambda: 0)
            for fileid in fileids:
                if fileid in training_set:
                    num_files += 1
                    for token in BROWN.words(fileids=fileid):
                        already_seen = set()
                        if not token in already_seen:
                            already_seen.add(token)
                            counts[token] += 1
            token_counts[genre] = counts
            genre_counts[genre] = num_files
        self.token_counts = token_counts
        self.genre_counts = genre_counts

    def create_classifier(self, k=1):
        def nb_classifier(test_example):
            # We add k virtual pos and neg articles for each token.
            # log(P(f|c)) = log(count(f,c)/count(c)) = log(count(f,c)) - log(count(c))
            log_prob_totals = defaultdict(lambda: 0.0)
            tc = self.token_counts
            min_nlp = float('inf') # Negative log probability.
            second_lowest = float('inf')
            c = None
            for genre in GENRES:
                already_seen = set()
                lgenre = log(self.genre_counts[genre] + k * len(tc[genre]))
                total_nlp = 0.0
                for token in test_example:
                    if not token in already_seen:
                        already_seen.add(token)
                        total_nlp += lgenre - log(tc[genre][token] + k) if tc[genre][token] + k > 0 else float('inf')
                if total_nlp < min_nlp:
                    second_lowest = min_nlp
                    min_nlp = total_nlp
                    c = genre
            return c if second_lowest > min_nlp else TIE
        return nb_classifier

