import json
import nltk
import os
import subprocess

BROWN = nltk.corpus.brown

def prep_brown():
    if not os.path.exists('concepts'):
        os.makedirs('concepts')
    for fid in BROWN.fileids():
        paragraph = ' '.join(BROWN.words(fileids=fid))
        paragraph_concepts = get_concepts(process_response(response))
        sentences = paragraph.split('.')
        sentence_concepts = reduce(lambda x,y: x + y,\
                map(lambda sentence: process_response(get_concepts(sentence)),\
                sentences))
        with open("concepts/%s_concepts.json", 'w'):
            json.dumps({'para': paragraph_concepts,\
                    'sent': sentence_concepts})

def get_concepts(text):
    p = subprocess.Popen(["java", "-cp", "esa-lucene.jar",\
            "edu.wiki.demo.TestESAVectors"])
    (response, err) = p.communicate(input=text)
    return response

def process_response(response):
    return map(lambda line: int(line.split()[0]), response)

if __name__ == "__main__":
    with open('tmp.txt', 'r') as f:
        print process_response(f.readlines())

